const express = require('express');
const bodyParser = require('body-parser');
const { spawnSync } = require('child_process');
const commandExistsSync = require('command-exists').sync;

const port = 63000;
const path_to_jq='jq';
const required_major = 1;
const required_minor = 6;

// check if jq exists and is 1.6
if (commandExistsSync('jq')) {
  let test_jq = spawnSync(path_to_jq, ['--version']);
  let output = test_jq.stdout.toString();
  let index_of_dot = output.indexOf('.');

  let major = parseInt(output[index_of_dot - 1]);
  let minor = parseInt(output[index_of_dot + 1]);

  if (major < required_major || minor < required_minor) {
    console.error('jq is not of the requred version (1.6). Please install the required version.');
    process.exit(1);
  }
}
else {
  console.error('jq was not found, please install it.');
  process.exit(1);
}

const app = express();

var isWin = /^win/.test(process.platform);

// replace all proto
String.prototype.replaceAll = function(search, replace)
{
    //if replace is not sent, return original string otherwise it will
    //replace search string with 'undefined'.
    if (replace === undefined) {
        console.error('Replace is undefined..');
        process.exit(1);
    }

    return this.replace(new RegExp('[' + search + ']', 'g'), replace);
};

// convert stream to string
async function streamToString (stream) {
  const chunks = []
  return new Promise((resolve, reject) => {
    stream.on('data', chunk => chunks.push(chunk))
    stream.on('error', reject)
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')))
  })
}

app.use(bodyParser.json({limit: '50mb'})); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' })); // for parsing application/x-www-form-urlencoded
app.use(express.static('public'));

app.post('/jq', (request, response) => {
  var _script = '';
  if (isWin === true) {
    _script = '\"' + request.body.script.replaceAll('"', '\\"') + '\"';
  }
  else {
    _script = ' \'' + request.body.script.replaceAll('\'', '\\\'') + '\'';
  }
  
  raw_output = '';
  if (request.body.is_raw_output == 'true') {
    raw_output = '-r';
  }
  
  sort_keys = '';
  if (request.body.is_sort_keys == 'true') {
    sort_keys = '--sort-keys';
  }
  
  const child = spawnSync(path_to_jq, [raw_output, sort_keys, _script], {input: request.body.input, encoding:"utf8", shell:true})
  
  response.json({ out: child.stdout, err: child.stderr });
  
});

app.listen(port, (err) => {
  if (err) {
    return console.error('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
});