function resize_elements() {
  window_h = $(window).innerHeight();
  topbar_div_h = $('#topbar_div').outerHeight();
  input_div_h = $('#input_div').outerHeight();
  script_div_h = $('#script_div').outerHeight();
  big_container_h = $('#big_container').outerHeight() - $('#big_container').height();
  
  taH = window_h - (topbar_div_h + big_container_h + script_div_h) - 45;
  $('#json_input').height(taH).show();
  $('#json_output').height(window_h - topbar_div_h - 40).show();
}

function clear_all() {
  $('#script_input').val('');
  $('#json_input').val('');
  $('#json_output').val('');
  $('#json_output').css('border-color', 'transparent');
  
  if ($('#is_raw_output').is(':checked')) {
      $('#is_raw_output_button').click();
  }
  
  if ($('#is_sort_keys').is(':checked')) {
      $('#is_sort_keys_button').click();
  }
  resize_elements();
  $('#json_input').focus();
}
    
$(document).ready(function(){
  resize_elements();
  for (let l = 0; l < 5; l += 1) {
    $('#button_append_' + l).click(function(){
      $('#script_input').val($('#script_input').val() + ' | ' + $('#example_output_' + l).text());
    });
    $('#button_replace_' + l).click(function(){
      $('#script_input').val($('#example_output_' + l).text());
    });
  }
});
    
$(document).resize(function(){
  resize_elements();
});

$(window).resize(function(){
  resize_elements();
});

$('#script_input').on('autosize:resized', function(){
  resize_elements();
});
    
$( function() {
   $('#run_it').click( function(event) {
     event.preventDefault();
     postdata = {}
     postdata.script = $('#script_input').val().length > 0 ? 
                        $('#script_input').val() : 
                        $('#script_input').attr('placeholder');
     postdata.input = $('#json_input').val();
     postdata.is_raw_output = $('#is_raw_output').is(':checked');
     postdata.is_sort_keys = $('#is_sort_keys').is(':checked');
     console.log(postdata);
     $.post('/jq', postdata, function(data, status) {
       if (data.err.length > 1) {
         $('#json_output').css('border-color', 'red');
         $('#json_output').val(data.err);
       }
       else {
         $('#json_output').css('border-color', 'green');
         $('#json_output').val(data.out);
       }
     });
   }); 
});
    
$('#reset_all').click( function(event) {
  event.preventDefault();
  clear_all();
});
    
document.addEventListener("DOMContentLoaded", function() {
  var inputs = document.querySelectorAll('[readonly]');
  for(var i=0; i < inputs.length; i++){
    inputs[i].addEventListener('keydown', function(e){
      var key = e.which || e.keyCode || 0;
      if(key === 8){
        e.preventDefault();
      }
    })
  }
});
    
$('#examples_modal_toggle').click(function(){
  $('#examples_modal').modal('toggle');
});